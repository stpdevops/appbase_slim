<!DOCTYPE html>
<html lang="en">
<head>
<title>Cart</title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">

<meta name="description" content="OneTech shop project">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" type="text/css" href="{{ asset('OneTech/styles/bootstrap4/bootstrap.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('OneTech/plugins/fontawesome-free-5.0.1/css/fontawesome-all.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('OneTech/styles/cart_styles.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('OneTech/styles/cart_responsive.css') }}">
{{-- <link rel="stylesheet" type="text/css" href="{{ asset('OneTech/styles/main_styles.css') }}"> --}}


</head>

<body>

<div class="super_container">

	<!-- Header -->

	<header class="header">

		<!-- Top Bar -->

		<div class="top_bar">
			<div class="container">
				<div class="row">
					<div class="col d-flex flex-row">
						<div class="top_bar_content ml-auto">
							<div class="top_bar_user">
								<a data-toggle="modal" data-target="#modal-login" href="#"><img src="{{ asset('OneTech/images/users.png') }}" alt="เข้าสู่ระบบ" onerror="this.src='{{ asset('OneTech/images/user.svg') }}'" width="40" height="35">
									<div>เข้าสู่ระบบ</div>
								</a>
								<div><a href="users/register">สมัครสมาชิกฟรี</a></div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<!-- Header Main -->

		<div class="header_main">
			<div class="container">
				<div class="row">

					<!-- Logo -->
					<div class="col-lg-2 col-sm-3 col-3 order-1">
						<div class="index_logo_container">
							<div class="logo"><a href="index">OneTech</a></div>
							{{-- <p id="demo" ></p> --}}
						</div>
					</div>

				</div>
			</div>
		</div>

		<!-- Main Navigation -->

		<nav class="main_nav">
			<div class="container">
				<div class="row">
					<div class="col">

						<div class="main_nav_content d-flex flex-row">

							<!-- Categories Menu -->

							<div class="cat_menu_container">
								<div class="cat_menu_title d-flex flex-row align-items-center justify-content-start">
									<div class="cat_burger"><span></span><span></span><span></span></div>
									<div class="cat_menu_text">ข่าวเพิ่มเติม</div>
								</div>

								<ul class="cat_menu">
									<li><a href="#">แนะนำเพื่อนรับเงินฟรี 10% <i class="fas fa-chevron-right ml-auto"></i></a></li>
									<li><a href="#">วิธีฝากเงินเข้าสู่ระบบ <i class="fas fa-chevron-right ml-auto"></i></a></li>
								</ul>
							</div>

							<!-- Main Nav Menu -->

							<div class="main_nav_menu ml-auto">
								<ul class="standard_dropdown main_nav_dropdown">

									<i class="fa fa-trophy" aria-hidden="true"></i>
									<li class="hassubs">
										<a href="#">ตรวจสอบผลสลาก<i class="fas fa-chevron-down"></i></a>
										<ul>
											<li><a href="#">หวยรัฐบาลไทย<i class="fas fa-chevron-down"></i></a></li>
											<li><a href="#">หวยรายวัน<i class="fas fa-chevron-down"></i></a></li>
										</ul>
									</li>

									<li><a class="far fa-address-card" href="#"> ติดต่อแอดมิน<i class="fas fa-chevron-down"></i></a></li>
								</ul>
							</div>

							<!-- Menu Trigger -->

							<div class="menu_trigger_container ml-auto">
								<div class="menu_trigger d-flex flex-row align-items-center justify-content-end">
									<div class="menu_burger">
										<div class="menu_trigger_text">เมนู</div>
										<div class="cat_burger menu_burger_inner"><span></span><span></span><span></span></div>
									</div>
								</div>
							</div>

						</div>
					</div>
				</div>
			</div>
		</nav>

		<!-- Menu -->

		<div class="page_menu">
			<div class="container">
				<div class="row">
					<div class="col">

						<div class="page_menu_content">

							<ul class="page_menu_nav">
								<li class="page_menu_item"><a data-toggle="modal" data-target="#modal-login" href="#"> เข้าสู่ระบบ<i class="fa fa-angle-down"></i></a></li>
								<li class="page_menu_item"><a href="users/register"> สมัครสมาชิกฟรี<i class="fa fa-angle-down"></i></a></li>
								<li class="page_menu_item has-children">
									<a href="#"> ตรวจสอบผลสลาก<i class="fa fa-angle-down"></i></a>
									<ul class="page_menu_selection">
										<li><a href="#">หวยรัฐบาลไทย<i class="fa fa-angle-down"></i></a></li>
										<li><a href="#">หวยรายวัน<i class="fa fa-angle-down"></i></a></li>
									</ul>
								</li>
								<li class="page_menu_item"><a href="#"> ติดต่อแอดมิน<i class="fa fa-angle-down"></i></a></li>
							</ul>

						</div>
					</div>
				</div>
			</div>
		</div>

	</header>

	<!-- Blog Posts -->

	<div class="index_blog">
		<div class="container">
			<div class="row">
				<div class="col-lg-10 offset-lg-1">
					<div class="blog_posts d-flex flex-row align-items-start justify-content-between">

						<!-- Blog post -->
						<div class="blog_post col-6 col-sm-6 col-md-5 col-lg-4">
							<a href="#">
								<div class="blog_image" style="background-image:url({{ asset('OneTech/images/หวยออนไลน์.jpg') }})"></div>
							</a>
							<div class="ribbon ribbon-top-left"><span>HOT</span></div>
							<div class="blog_text">หวยรัฐบาลไทย</div>
							<div class="blog_button"><a href="#">อ่านต่อไป</a></div>
						</div>

						<!-- Blog post -->
						<div class="blog_post col-6 col-sm-6 col-md-5 col-lg-4">
							<a href="#">
								<div class="blog_image" style="background-image:url({{ asset('OneTech/images/จับยี่กี.jpg') }})"></div>
							</a>
							<div class="blog_text">หวยรายวัน (จับยี่กี VIP)</div>
							<div class="blog_button"><a href="#">อ่านต่อไป</a></div>
						</div>

						<!-- Blog post -->
						<div class="blog_post col-6 col-sm-6 col-md-5 col-lg-4">
							<a href="#">
								<div class="blog_image" style="background-image:url({{ asset('OneTech/images/game_coin.jpg') }})"></div>
							</a>
							<div class="blog_text">เป่ายิงฉุบ & หัวก้อย</div>
							<div class="blog_button"><a href="#">อ่านต่อไป</a></div>
						</div>

					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- Cart -->

	<div class="index_cart_section">
		<div class="container">
			<div class="row">
				<div class="col-lg-10 offset-lg-1">
					<div class="cart_container">
						<div class="cart_title">รายการน่าสนใจ</div>

						<div class="index_cart_items">
							<ul class="cart_list">
								<li class="index_cart_item clearfix">

									<h6><i class="fas fa-trophy"></i> จับยี่กี VIP <span class="badge badge-dark">29 August 2019</span></h6>

									<div class="text-danger text-center">
										<div class="dd-box dd-gold">
											<div class="row d-flex justify-content-center">
												<div class="col-12 col-sm-12 col-md-6">
													<img class="col-12 col-sm-12 col-md-12" src="https://www.lottovip.com/assets/images/jackpot.png" alt="">
													<div class="col-12 col-sm-12 col-md-12">ฝาก-ถอน ไม่มีขั้นต่ำ </div>
												</div>
											</div>
										</div>
									</div>


									<div class="container">
										  <h2>ผลออกรางวัลหวยรายวัน</h2>
										  <ul class="nav nav-pills mb-3 w-100 bg-white rounded p-1">
										    <li class="nav-item w-50"><a class="nav-link btn btn-outline-dark btn-sm mx-1 active show" data-toggle="tab" href="#home">แบบย่อ</a></li>
										    <li class="nav-item w-50"><a class="nav-link btn btn-outline-dark btn-sm mx-1" data-toggle="tab" href="#menu1">ทั้งหมด</a></li>
										  </ul>

										  <div class="tab-content">
										    <div id="home" class="tab-pane fade in active show">
										      {{-- <h3>HOME</h3> --}}
													<!-- Mobile App -->

																<div class="bg-danger p-1 my-1 text-center text-white"></div>
																<div id="myCarousel2" class="carousel slide" data-ride="carousel">

																		<div class="d-flex justify-content-between py-1">
																				<a href="#myCarousel2" role="button" data-slide="prev" class="btn btn-dark btn-sm">
																						<span><i class="fas fa-chevron-left"></i> ก่อนหน้า</span>
																				</a>

																				<a href="#myCarousel2" role="button" data-slide="next" class="btn btn-dark btn-sm">
																						<span> ถัดไป <i class="fas fa-chevron-right"></i></span>
																				</a>
																		</div>

																		<div class="carousel-inner" role="listbox">
																				<div class="carousel-item active">
																							<div class="card border-secondary text-center mb-2">
																									<div class="card-header text-danger p-1">
																									จับยี่กี VIP - รอบที่ 1 </div>
																											<div class="card-body p-0">
																														<div class="d-flex flex-row">
																																	<div class="card text-center w-50 border-card-right m-0">
																																				<div class="card-header sub-card-header bg-transparent p-0">
																																				3ตัวบน<br>

																																				</div>
																																				<div class="card-body p-0">
																																				<p class="card-text">239</p>
																																				</div>
																																	</div>
																																	<div class="card text-center w-50 border-card-right m-0">
																																	<div class="card-header sub-card-header bg-transparent p-0">
																																	2ตัวล่าง<br>

																																	</div>
																																				<div class="card-body p-0">
																																				<p class="card-text">76</p>
																																				</div>
																																	</div>
																														</div>
																											</div>
																							</div>
																				</div>
																				<div class="carousel-item">
																							<div class="card border-secondary text-center mb-2">
																									<div class="card-header text-danger p-1">
																									จับยี่กี VIP - รอบที่ 2 </div>
																											<div class="card-body p-0">
																														<div class="d-flex flex-row">
																																	<div class="card text-center w-50 border-card-right m-0">
																																				<div class="card-header sub-card-header bg-transparent p-0">
																																				3ตัวบน<br>

																																				</div>
																																				<div class="card-body p-0">
																																				<p class="card-text">239</p>
																																				</div>
																																	</div>
																																	<div class="card text-center w-50 border-card-right m-0">
																																	<div class="card-header sub-card-header bg-transparent p-0">
																																	2ตัวล่าง<br>

																																	</div>
																																				<div class="card-body p-0">
																																				<p class="card-text">76</p>
																																				</div>
																																	</div>
																														</div>
																											</div>
																							</div>
																				</div>
																				<div class="carousel-item">
																							<div class="card border-secondary text-center mb-2">
																									<div class="card-header text-danger p-1">
																									จับยี่กี VIP - รอบที่ 3 </div>
																											<div class="card-body p-0">
																														<div class="d-flex flex-row">
																																	<div class="card text-center w-50 border-card-right m-0">
																																				<div class="card-header sub-card-header bg-transparent p-0">
																																				3ตัวบน<br>

																																				</div>
																																				<div class="card-body p-0">
																																				<p class="card-text">239</p>
																																				</div>
																																	</div>
																																	<div class="card text-center w-50 border-card-right m-0">
																																	<div class="card-header sub-card-header bg-transparent p-0">
																																	2ตัวล่าง<br>

																																	</div>
																																				<div class="card-body p-0">
																																				<p class="card-text">76</p>
																																				</div>
																																	</div>
																														</div>
																											</div>
																							</div>
																				</div>
																		</div>
																</div>
													<!-- End Mobile App -->
										    </div>
										    <div id="menu1" class="tab-pane fade">
										      {{-- <h3>Menu 1</h3> --}}
													<!-- Win App -->

															<div class="row px-0 m-0">
																	<div class="col-12 col-sm-12 col-md-6 col-lg-3 px-1">
																			<div class="card border-secondary text-center mb-2">
																				<div class="card-header text-danger p-1">
																				จับยี่กี VIP - รอบที่ 1 </div>
																							<div class="card-body p-0">
																									<div class="d-flex flex-row">
																											<div class="card text-center w-50 border-card-right m-0">
																													<div class="card-header sub-card-header bg-transparent p-0">
																													3ตัวบน<br>

																													</div>
																													<div class="card-body p-0">
																													<p class="card-text">239</p>
																													</div>
																											</div>
																											<div class="card text-center w-50 border-card-right m-0">
																													<div class="card-header sub-card-header bg-transparent p-0">
																													2ตัวล่าง<br>

																													</div>
																													<div class="card-body p-0">
																													<p class="card-text">76</p>
																													</div>
																											</div>
																									</div>
																						</div>
																			</div>
																	</div>

																	<div class="col-12 col-sm-12 col-md-6 col-lg-3 px-1">
																			<div class="card border-secondary text-center mb-2">
																				<div class="card-header text-danger p-1">
																				จับยี่กี VIP - รอบที่ 2 </div>
																							<div class="card-body p-0">
																									<div class="d-flex flex-row">
																											<div class="card text-center w-50 border-card-right m-0">
																													<div class="card-header sub-card-header bg-transparent p-0">
																													3ตัวบน<br>

																													</div>
																													<div class="card-body p-0">
																													<p class="card-text">965</p>
																													</div>
																											</div>
																											<div class="card text-center w-50 border-card-right m-0">
																													<div class="card-header sub-card-header bg-transparent p-0">
																													2ตัวล่าง<br>

																													</div>
																													<div class="card-body p-0">
																													<p class="card-text">58</p>
																													</div>
																											</div>
																									</div>
																						</div>
																			</div>
																	</div>

																	<div class="col-12 col-sm-12 col-md-6 col-lg-3 px-1">
																			<div class="card border-secondary text-center mb-2">
																				<div class="card-header text-danger p-1">
																				จับยี่กี VIP - รอบที่ 3 </div>
																							<div class="card-body p-0">
																									<div class="d-flex flex-row">
																											<div class="card text-center w-50 border-card-right m-0">
																													<div class="card-header sub-card-header bg-transparent p-0">
																													3ตัวบน<br>

																													</div>
																													<div class="card-body p-0">
																													<p class="card-text">xxx</p>
																													</div>
																											</div>
																											<div class="card text-center w-50 border-card-right m-0">
																													<div class="card-header sub-card-header bg-transparent p-0">
																													2ตัวล่าง<br>

																													</div>
																													<div class="card-body p-0">
																													<p class="card-text">xx</p>
																													</div>
																											</div>
																									</div>
																						</div>
																			</div>
																	</div>
															</div>
													<!-- End Win App -->
										    </div>

										 </div>
									</div>

								</li>
							</ul>
						</div>


						<div class="cart_buttons">
							<button type="button" class="button cart_button_clear">Add to Cart</button>
							<button type="button" class="button cart_button_checkout">Add to Cart</button>
						</div>

					</div>
				</div>
		  </div>
	  </div>
  </div>


	<div class="index_cart_section">
		<div class="container">
			<div class="row">
				<div class="col-lg-10 offset-lg-1">
					<div class="cart_container">
						<div class="cart_title">รายการน่าสนใจ</div>

						<div class="index_cart_items">
								 <ul class="cart_list">
										 <li class="index_cart_item clearfix">
											 <h6><i class="fas fa-trophy"></i> ออกรางวัลครั้ง <span class="badge badge-dark">29 August 2019</span></h6>

																	 <div class="row d-flex justify-content-center">
																				 <div class="col-10 col-sm-10 col-md-10 col-lg-10 px-1">

																							 <div class="deals_timer_title_container">
																										<div class="deals_timer_title">นับถอยหลังออกรางวัล รอบที่ 3</div>
																										<div class="deals_timer_subtitle">ปิดรับเวลา 00.00.00</div>
																								</div>

																								<div class="deals_timer_content ml-auto">
																										<div class="deals_timer_box clearfix" data-target-time="2019-08-31 01:30:51.01">

																												<div class="deals_timer_unit">
																													<div id="deals_timer1_hr" class="deals_timer_hr">15</div>
																													<span>ชั่วโมง</span>
																												</div>

																												<div class="deals_timer_unit">
																													<div id="deals_timer1_min" class="deals_timer_min">23</div>
																													<span>นาที</span>
																												</div>

																												<div class="deals_timer_unit">
																													<div id="deals_timer1_sec" class="deals_timer_sec">43</div>
																													<span>วินาที</span>
																												</div>

																										</div>
																								</div>

																				</div>
																	 </div>

										 </li>
								 </ul>
					 </div>

					</div>
				</div>
			</div>
		</div>
	</div>






	<!-- Newsletter -->

	<div class="newsletter">
		<div class="container">
			<div class="row">
				<div class="col">
					<div class="newsletter_container d-flex flex-lg-row flex-column align-items-lg-center align-items-center justify-content-lg-start justify-content-center">
						<div class="newsletter_title_container">
							<div class="newsletter_icon"><img src="{{ asset('OneTech/images/send.png') }}" alt=""></div>
							<div class="newsletter_title">Sign up for Newsletter</div>
							<div class="newsletter_text"><p>...and receive %20 coupon for first shopping.</p></div>
						</div>
						<div class="newsletter_content clearfix">
							<form action="#" class="newsletter_form">
								<input type="email" class="newsletter_input" required="required" placeholder="Enter your email address">
								<button class="newsletter_button">Subscribe</button>
							</form>
							<div class="newsletter_unsubscribe_link"><a href="#">unsubscribe</a></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- Footer -->

	<div class="custom_dropdown col-lg-0 col-0">
		<div class="custom_dropdown_list">
			<span class="custom_dropdown_placeholder clc"></span>
			<ul class="custom_list clc">
			</ul>
		</div>
	</div>

	<footer class="footer">
		<div class="container">
			<div class="row">

				<div class="col-lg-3 footer_col">
					<div class="footer_column footer_contact">
						<div class="logo_container">
							<div class="logo"><a href="#">OneTech</a></div>
						</div>
						<div class="footer_title">Got Question? Call Us 24/7</div>
						<div class="footer_phone">+38 068 005 3570</div>
						<div class="footer_contact_text">
							<p>17 Princess Road, London</p>
							<p>Grester London NW18JR, UK</p>
						</div>
						<div class="footer_social">
							<ul>
								<li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
								<li><a href="#"><i class="fab fa-twitter"></i></a></li>
								<li><a href="#"><i class="fab fa-youtube"></i></a></li>
								<li><a href="#"><i class="fab fa-google"></i></a></li>
								<li><a href="#"><i class="fab fa-vimeo-v"></i></a></li>
							</ul>
						</div>
					</div>
				</div>

				<div class="col-lg-2 offset-lg-2">
					<div class="footer_column">
						<div class="footer_title">Find it Fast</div>
						<ul class="footer_list">
							<li><a href="#">Computers & Laptops</a></li>
							<li><a href="#">Cameras & Photos</a></li>
							<li><a href="#">Hardware</a></li>
							<li><a href="#">Smartphones & Tablets</a></li>
							<li><a href="#">TV & Audio</a></li>
						</ul>
						<div class="footer_subtitle">Gadgets</div>
						<ul class="footer_list">
							<li><a href="#">Car Electronics</a></li>
						</ul>
					</div>
				</div>

				<div class="col-lg-2">
					<div class="footer_column">
						<ul class="footer_list footer_list_2">
							<li><a href="#">Video Games & Consoles</a></li>
							<li><a href="#">Accessories</a></li>
							<li><a href="#">Cameras & Photos</a></li>
							<li><a href="#">Hardware</a></li>
							<li><a href="#">Computers & Laptops</a></li>
						</ul>
					</div>
				</div>

				<div class="col-lg-2">
					<div class="footer_column">
						<div class="footer_title">Customer Care</div>
						<ul class="footer_list">
							<li><a href="#">My Account</a></li>
							<li><a href="#">Order Tracking</a></li>
							<li><a href="#">Wish List</a></li>
							<li><a href="#">Customer Services</a></li>
							<li><a href="#">Returns / Exchange</a></li>
							<li><a href="#">FAQs</a></li>
							<li><a href="#">Product Support</a></li>
						</ul>
					</div>
				</div>

			</div>
		</div>
	</footer>


	<!--  modal login-->
	<div id="modal-login" class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	  <div class="modal-dialog modal-dialog-centered" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <h5 class="modal-title" id="exampleModalLongTitle">ลงชื่อเข้าใช้</h5>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	      </div>
	      <div class="modal-body">

					<form action="users/checklogin" id="loginForm" method="post">
							<div class="form-group">
									{!! csrf !!}
									<label class="control-label" for="username">ชื่อผู้ใช้งาน</label>
									<input type="text" placeholder="example@gmail.com" title="Please enter you username" required="" value="" name="username" id="username" class="form-control">
									<span class="help-block small">ชื่อผู้ใช้งานของคุณ</span>
							</div>
							<div class="form-group">
									<label class="control-label" for="password">รหัสผ่าน</label>
									<input type="password" title="Please enter your password" placeholder="******" required="" value="" name="password" id="password" class="form-control">
									<span class="help-block small">รหัสผ่านของคุณ</span>
							</div>
							<div class="form-group">
								<a href="/index">ลืมรหัสผ่าน</a>
							</div>
							<div class="checkbox login-checkbox">
									<label><input type="checkbox" class="i-checks"> จดจำฉัน </label>
							</div>
							<button class="btn btn-success btn-block loginbtn">เข้าสู่ระบบ</button>
							<input  class="btn btn-default btn-block" type="button" onclick="location.href='users/register'" value="สมัครสมาชิก"/>
					</form>

				</div>

	    </div>
	  </div>
	</div>

	<!-- Copyright -->

	<div class="copyright">
		<div class="container">
			<div class="row">
				<div class="col">

					<div class="copyright_container d-flex flex-sm-row flex-column align-items-center justify-content-start">
						<div class="copyright_content"><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="fa fa-heart" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a>
<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
</div>
						<div class="logos ml-sm-auto">
							<ul class="logos_list">
								<li><a href="#"><img src="{{ asset('OneTech/images/logos_1.png') }}" alt=""></a></li>
								<li><a href="#"><img src="{{ asset('OneTech/images/logos_2.png') }}" alt=""></a></li>
								<li><a href="#"><img src="{{ asset('OneTech/images/logos_3.png') }}" alt=""></a></li>
								<li><a href="#"><img src="{{ asset('OneTech/images/logos_4.png') }}" alt=""></a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script src="{{ asset('OneTech/js/jquery-3.3.1.min.js') }}"></script>
<script src="{{ asset('OneTech/styles/bootstrap4/popper.js') }}"></script>
<script src="{{ asset('OneTech/styles/bootstrap4/bootstrap.min.js') }}"></script>
<script src="{{ asset('OneTech/plugins/greensock/TweenMax.min.js') }}"></script>
<script src="{{ asset('OneTech/plugins/greensock/TimelineMax.min.js') }}"></script>
<script src="{{ asset('OneTech/plugins/scrollmagic/ScrollMagic.min.js') }}"></script>
<script src="{{ asset('OneTech/plugins/greensock/animation.gsap.min.js') }}"></script>
<script src="{{ asset('OneTech/plugins/greensock/ScrollToPlugin.min.js') }}"></script>
<script src="{{ asset('OneTech/plugins/easing/easing.js') }}"></script>
<script src="{{ asset('OneTech/js/cart_custom.js') }}"></script>
<script src="{{ asset('OneTech/js/custom.js') }}"></script>
</body>

</html>
