<?php
namespace App\controllers;

use App\Models\Users;
use App\controllers\Controller;
use Illuminate\Database\Capsule\Manager as DB;

class IndexController extends Controller
{
    /**
     * Homepage
     */

    public function Index($request, $response, $args)
    {

      if($this->container->Auth->Adminauth()){
        // Check Admin Authen
        return $response->withRedirect('/admin/index');
      }

      if($this->container->Auth->validateAuth()){
        // Check User Authen
        return $response->withRedirect('/users/index');
      }

      return view('index');
    }

}
